package com.dain_torson.servlets.soap.info;

import com.dain_torson.httpclient.data.SimplePersonData;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use= SOAPBinding.Use.LITERAL)
public interface SOAPInfoService {

    @WebMethod
    List<SimplePersonData> getAll();
    @WebMethod
    List<SimplePersonData> getByFio(String fio);
    @WebMethod
    List<SimplePersonData> getByPattern(SimplePersonData pattern);
    @WebMethod
    boolean newRecord(SimplePersonData data);
    @WebMethod
    boolean deleteRecord(SimplePersonData pattern);
    @WebMethod
    boolean editRecord(SimplePersonData oldData, SimplePersonData newData);
}
