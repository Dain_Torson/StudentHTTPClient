package com.dain_torson.httpclient.view;


import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

import java.util.ArrayList;

public class GUIManager {

    private Label statusLabel;
    private TablePane resultTable;
    private MenuItem connectItem;
    private MenuItem disconnectItem;
    private TextField nameField;

    public GUIManager(Label statusLabel, TablePane resultTable, MenuItem connectItem, MenuItem disconnectItem,
                      TextField nameField) {
        this.statusLabel = statusLabel;
        this.resultTable = resultTable;
        this.connectItem = connectItem;
        this.disconnectItem = disconnectItem;
        this.nameField = nameField;
    }

    public void clearGui(){
        statusLabel.setText("No connection");
        resultTable.setPersonData(new ArrayList<>());
        connectItem.setDisable(false);
        disconnectItem.setDisable(true);
        nameField.setText("");
    }

    public void prepareGui(String url, int port) {
        connectItem.setDisable(true);
        disconnectItem.setDisable(false);
        statusLabel.setText("Connected to: " + url + ":" + String.valueOf(port));
        nameField.setText("");
    }
}
