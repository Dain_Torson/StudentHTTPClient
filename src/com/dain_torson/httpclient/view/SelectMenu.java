package com.dain_torson.httpclient.view;


import com.dain_torson.httpclient.data.PersonDataContainer;
import javafx.scene.control.ContextMenu;

public class SelectMenu extends ContextMenu{

    PersonDataContainer lastSelected;

    public PersonDataContainer getLastSelected() {
        return lastSelected;
    }

    public void setLastSelected(PersonDataContainer lastSelected) {
        this.lastSelected = lastSelected;
    }
}
