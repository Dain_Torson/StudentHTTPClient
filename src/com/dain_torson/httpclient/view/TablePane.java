package com.dain_torson.httpclient.view;

import com.dain_torson.httpclient.data.PersonDataContainer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import java.util.List;


public class TablePane extends Pane {

    private TableView<PersonDataContainer> personTable;

    public TablePane() {
        initTable();
    }

    private void initTable() {

        personTable = new TableView<>();

        TableColumn<PersonDataContainer, String> fioColumn = new TableColumn<>();
        fioColumn.setCellValueFactory(new PropertyValueFactory<>("fio"));
        fioColumn.setText("Name");
        fioColumn.setPrefWidth(114);

        TableColumn<PersonDataContainer, String> groupColumn = new TableColumn<>();
        groupColumn.setCellValueFactory(new PropertyValueFactory<>("group"));
        groupColumn.setText("Group");
        groupColumn.setPrefWidth(114);

        TableColumn<PersonDataContainer, String> eduColumn = new TableColumn<>();
        eduColumn.setCellValueFactory(new PropertyValueFactory<>("eduForm"));
        eduColumn.setText("Ed.Form");
        eduColumn.setPrefWidth(114);

        TableColumn<PersonDataContainer, String> genderColumn = new TableColumn<>();
        genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));
        genderColumn.setText("Gender");
        genderColumn.setPrefWidth(114);

        TableColumn<PersonDataContainer, String> cityColumn = new TableColumn<>();
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        cityColumn.setText("City");
        cityColumn.setPrefWidth(114);

        TableColumn<PersonDataContainer, Integer> ageColumn = new TableColumn<PersonDataContainer, Integer>();
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        ageColumn.setPrefWidth(114);
        ageColumn.setText("Age");

        TableColumn<PersonDataContainer, Double> grageColumn = new TableColumn<PersonDataContainer, Double>();
        grageColumn.setCellValueFactory(new PropertyValueFactory<>("avgGrade"));
        grageColumn.setPrefWidth(114);
        grageColumn.setText("Avg.Grade");

        personTable.getColumns().addAll(fioColumn, groupColumn, eduColumn, genderColumn,
                cityColumn, ageColumn, grageColumn);
        getChildren().add(personTable);
    }

    public void setPersonData(List<PersonDataContainer> persons) {
        ObservableList<PersonDataContainer> personsList = FXCollections.observableArrayList(persons);
        personTable.setItems(personsList);
    }

    public TableView<PersonDataContainer> getPersonTable() {
        return personTable;
    }
}
