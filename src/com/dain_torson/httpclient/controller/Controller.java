package com.dain_torson.httpclient.controller;

import com.dain_torson.httpclient.controller.connection.ConnectionManager;
import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.msgbox.ConfirmMsgBoxController;
import com.dain_torson.httpclient.controller.msgbox.ProcessRecordMsgBoxController;
import com.dain_torson.httpclient.controller.msgbox.ConnectMsgBoxController;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.view.GUIManager;
import com.dain_torson.httpclient.view.SelectMenu;
import com.dain_torson.httpclient.view.TablePane;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class Controller {

    private Stage primaryStage;
    private ConnectionManager manager;
    private GUIManager guiManager;

    private SelectMenu contextMenu;

    @FXML
    private TablePane resultTable;
    @FXML
    private MenuItem connectItem;
    @FXML
    private MenuItem disconnectItem;
    @FXML
    private TextField nameField;
    @FXML
    private Label statusLabel;

    @FXML
    private void initialize() {

        resultTable.getPersonTable().getSelectionModel()
                .selectedItemProperty().addListener(new TableListener());

        contextMenu = new SelectMenu();
        MenuItem editItem = new MenuItem("Edit");
        MenuItem deleteItem = new MenuItem("Delete");
        MenuItem closeItem = new MenuItem("Close");

        editItem.setOnAction(new EditItemHandler());
        deleteItem.setOnAction(new DeleteItemHandler());
        closeItem.setOnAction(event -> deselectAndClose());

        contextMenu.getItems().addAll(editItem, deleteItem, closeItem);
        contextMenu.setOnAutoHide(event -> deselectAndClose());

        guiManager = new GUIManager(statusLabel, resultTable, connectItem, disconnectItem, nameField);
        manager = new ConnectionManager(guiManager);
    }

    @FXML
    private void onConnectItemSelected() {
        if (manager.isConnected()) return;

        ConnectMsgBoxController msgBox = new ConnectMsgBoxController();
        msgBox.show(primaryStage);

        String url = msgBox.getUrl();
        int port = msgBox.getPort();
        ConnectionManager.ConnectionType type = msgBox.getType();

        if (type == null) return;

        try {
            manager.connect(type, url, port);
        }
        catch (Exception ex) {
            manager.handleError();
            return;
        }
        manager.getCurrentConnection().getAll(new UpdateGUIRunnableBuilder(resultTable));
        guiManager.prepareGui(url, port);
    }

    @FXML
    private void onDisconnectItemSelected() {
        if (!manager.isConnected()) return;
        manager.disconnect();
        guiManager.clearGui();
    }

    @FXML
    private void onExitItemSelected() {
        if(manager.isConnected()) {
            manager.disconnect();
        }
        Platform.exit();
    }

    @FXML
    private void onNewButtonPressed() {
        if(!manager.isConnected())return;
        ProcessRecordMsgBoxController msgBox = new ProcessRecordMsgBoxController();
        ProcessRecordMsgBoxController.setCreator(manager.getCurrentCreator());
        msgBox.show(primaryStage);

        PersonDataContainer container = msgBox.getEnteredData();
        if (container == null) return;

        manager.getCurrentConnection().newRecord(container, new UpdateGUIRunnableBuilder(resultTable));

    }

    @FXML
    private void onAdvancedButtonPressed() {
        if(!manager.isConnected())return;
        ProcessRecordMsgBoxController msgBox = new ProcessRecordMsgBoxController();
        ProcessRecordMsgBoxController.setCreator(manager.getCurrentCreator());
        msgBox.show(primaryStage);

        PersonDataContainer container = msgBox.getEnteredData();
        if (container == null) return;

        manager.getCurrentConnection().getByPattern(container, new UpdateGUIRunnableBuilder(resultTable));
    }

    @FXML
    private void onShowAllButtonPressed() {
        if(!manager.isConnected()) return;
        manager.getCurrentConnection().getAll(new UpdateGUIRunnableBuilder(resultTable));
    }

    @FXML
    private void onNameFieldKeyTyped() {
        if(!manager.isConnected())return;
        manager.getCurrentConnection().getByFio(nameField.getText(),
                new UpdateGUIRunnableBuilder(resultTable));
    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private class TableListener implements ChangeListener<PersonDataContainer> {

        @Override
        public void changed(ObservableValue<? extends PersonDataContainer> observable, PersonDataContainer oldValue,
                            PersonDataContainer newValue) {
            contextMenu.setLastSelected(newValue);
            contextMenu.show(resultTable, Side.RIGHT, -50, 25);
        }
    }

    private class EditItemHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            if (!manager.isConnected()) return;
            ProcessRecordMsgBoxController msgBox = new ProcessRecordMsgBoxController();
            PersonDataContainer pattern = contextMenu.getLastSelected();
            ProcessRecordMsgBoxController.setPattern(pattern);
            ProcessRecordMsgBoxController.setCreator(manager.getCurrentCreator());
            msgBox.show(primaryStage);

            PersonDataContainer container = msgBox.getEnteredData();
            if (container == null) return;

            manager.getCurrentConnection().editRecord(pattern, container, new UpdateGUIRunnableBuilder(resultTable));
        }
    }

    private class DeleteItemHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            if (!manager.isConnected()) return;
            PersonDataContainer target = contextMenu.getLastSelected();
            ConfirmMsgBoxController msgBox = new ConfirmMsgBoxController();
            msgBox.show(primaryStage);
            if (!msgBox.isConfirmed()) return;

            manager.getCurrentConnection().deleteRecord(target, new UpdateGUIRunnableBuilder(resultTable));
        }
    }

    public void deselectAndClose() {
        resultTable.getPersonTable().getSelectionModel().clearSelection();
        contextMenu.hide();
    }
}

