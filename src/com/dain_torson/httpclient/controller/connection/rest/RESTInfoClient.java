package com.dain_torson.httpclient.controller.connection.rest;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimpleDataWrapper;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class RESTInfoClient {

    private Client client;
    private WebTarget service;
    private static int SUCCESS = 201;

    public RESTInfoClient(URI uri) {

        ClientConfig config = new ClientConfig();
        client = ClientBuilder.newClient(config);
        service = client.target(uri);
    }

    public void close(){
        client.close();
    }

    public List<SimplePersonData> getAll() {
        return json2data(service.path("getAll").request().accept(MediaType.APPLICATION_JSON).get(String.class));
    }

    public List<SimplePersonData> getByFio(String fio) {
        return json2data(service.path("getByFio").request().accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(fio, MediaType.APPLICATION_XML), String.class));
    }

    public List<SimplePersonData> getByPattern(SimplePersonData pattern) {
        return json2data(service.path("getByPattern").request().accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(pattern, MediaType.APPLICATION_XML), String.class));
    }

    public boolean newRecord(SimplePersonData data) {
        Response response = service.path("newRecord").request(MediaType.TEXT_XML)
                .post(Entity.entity(data, MediaType.APPLICATION_XML), Response.class);
        return response.getStatus() == SUCCESS;
    }

    public boolean deleteRecord(SimplePersonData pattern) {
        Response response = service.path("deleteRecord").request(MediaType.TEXT_XML)
                .post(Entity.entity(pattern, MediaType.APPLICATION_XML), Response.class);
        return response.getStatus() == SUCCESS;
    }

    public boolean editRecord(SimplePersonData oldData, SimplePersonData newData) {
        SimpleDataWrapper wrapper = new SimpleDataWrapper();
        wrapper.setFirst(oldData);
        wrapper.setSecond(newData);
        Response response = service.path("editRecord").request(MediaType.TEXT_XML)
                .post(Entity.entity(wrapper, MediaType.APPLICATION_XML), Response.class);
        return response.getStatus() == SUCCESS;
    }

    private List<SimplePersonData> json2data(String source) {
        List<SimplePersonData> dataList = new ArrayList<>();

        JsonParser parser = new JsonParser();
        JsonArray mainObject = parser.parse(source).getAsJsonArray();
        for (JsonElement user : mainObject) {
            JsonObject userObject = user.getAsJsonObject();
            SimplePersonData data = new SimplePersonData();
            data.setFio(userObject.get("fio").getAsString());
            data.setGroup(userObject.get("group").getAsString());
            data.setGender(userObject.get("gender").getAsString());
            data.setEduForm(userObject.get("eduForm").getAsString());
            data.setCity(userObject.get("city").getAsString());
            data.setAge(userObject.get("age").getAsInt());
            data.setAvgGrade(userObject.get("avgGrade").getAsDouble());
            dataList.add(data);
        }
        return dataList;
    }
}
