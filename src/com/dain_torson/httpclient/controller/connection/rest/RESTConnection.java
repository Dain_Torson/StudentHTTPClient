package com.dain_torson.httpclient.controller.connection.rest;

import com.dain_torson.httpclient.controller.connection.Connection;
import com.dain_torson.httpclient.controller.connection.ConnectionManager;
import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rest.multithreading.*;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class RESTConnection extends Connection{

    private RESTInfoClient client;

    public RESTConnection(ConnectionManager manager) {
        super(manager);
    }

    @Override
    public void connect(String url, int port) throws Exception {
        String resourceUri =  url + ":" + port + "/StudentInfoServices/rest/info";
        URI uri = UriBuilder.fromUri(resourceUri).build();
        client = new RESTInfoClient(uri);
    }

    @Override
    public void disconnect() {
        client.close();
    }

    @Override
    protected Thread createGetAllThread(UpdateGUIRunnableBuilder builder) {
        return new RESTGetAllThread(client, builder);
    }

    @Override
    protected Thread createGetByFioThread(String fio, UpdateGUIRunnableBuilder builder) {
        return new RESTGetByFioThread(client, builder, fio);
    }

    @Override
    protected Thread createGetByPatternThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new RESTGetByPatternThread(client, builder, (SimplePersonData)pattern);
    }

    @Override
    protected Thread createNewRecordThread(PersonDataContainer data, UpdateGUIRunnableBuilder builder) {
        return new RESTNewRecordThread(client, builder, (SimplePersonData)data);
    }

    @Override
    protected Thread createDeleteRecordThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new RESTDeleteRecordThread(client, builder, (SimplePersonData)pattern);
    }

    @Override
    protected Thread createEditRecordThread(PersonDataContainer oldData, PersonDataContainer newData, UpdateGUIRunnableBuilder builder) {
        return new RESTEditRecordThread(client, builder, (SimplePersonData)oldData, (SimplePersonData) newData);
    }
}
