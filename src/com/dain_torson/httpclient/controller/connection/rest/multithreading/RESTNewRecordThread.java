package com.dain_torson.httpclient.controller.connection.rest.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rest.RESTInfoClient;
import com.dain_torson.httpclient.data.SimplePersonData;

import java.util.List;

public class RESTNewRecordThread extends RESTDataTransferThread{
    public RESTNewRecordThread(RESTInfoClient client, UpdateGUIRunnableBuilder builder, SimplePersonData param1) {
        super(client, builder, param1);
    }

    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) throws Exception {
        if(!client.newRecord((SimplePersonData)param1)) throw new Exception();
        return client.getAll();
    }
}
