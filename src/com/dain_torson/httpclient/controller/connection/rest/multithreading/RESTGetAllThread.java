package com.dain_torson.httpclient.controller.connection.rest.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rest.RESTInfoClient;
import com.dain_torson.httpclient.data.SimplePersonData;

import java.util.List;

public class RESTGetAllThread extends RESTDataTransferThread{
    public RESTGetAllThread(RESTInfoClient client, UpdateGUIRunnableBuilder builder) {
        super(client, builder, null);
    }

    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) throws Exception {
        return client.getAll();
    }
}
