package com.dain_torson.httpclient.controller.connection.rest.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rest.RESTInfoClient;
import com.dain_torson.httpclient.data.SimplePersonData;

import java.util.List;

public class RESTGetByPatternThread extends RESTDataTransferThread{
    public RESTGetByPatternThread(RESTInfoClient client, UpdateGUIRunnableBuilder builder, SimplePersonData param1) {
        super(client, builder, param1);
    }
    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) throws Exception {
        return client.getByPattern((SimplePersonData)param1);
    }
}
