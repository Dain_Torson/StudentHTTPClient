package com.dain_torson.httpclient.controller.connection.rest.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rest.RESTInfoClient;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import javafx.application.Platform;

import java.util.List;
import java.util.stream.Collectors;

public abstract class RESTDataTransferThread extends Thread{

    protected RESTInfoClient client;
    private UpdateGUIRunnableBuilder builder;
    private Object param1;
    private Object param2;

    public RESTDataTransferThread(RESTInfoClient client, UpdateGUIRunnableBuilder builder, Object param1) {
        this.client = client;
        this.param1 = param1;
        this.builder = builder;
    }

    public RESTDataTransferThread(RESTInfoClient client, UpdateGUIRunnableBuilder builder,
                                 Object param1, Object param2) {
        this.client = client;
        this.param1 = param1;
        this.param2 = param2;
        this.builder = builder;
    }

    protected abstract List<SimplePersonData> clientOperation(Object param1, Object param2) throws Exception;

    @Override
    public void run() {
        List<SimplePersonData> data;

        try {
            data = clientOperation(param1, param2);
        } catch (Exception ex) {
            throw new RuntimeException("Smth bad happened");
        }

        List<PersonDataContainer> result = data.stream().map(value -> (PersonDataContainer) value)
                .collect(Collectors.toList());
        builder.setPersonList(result);
        Platform.runLater(builder.build());
    }
}