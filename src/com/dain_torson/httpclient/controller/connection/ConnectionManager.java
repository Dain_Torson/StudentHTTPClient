package com.dain_torson.httpclient.controller.connection;


import com.dain_torson.httpclient.controller.Controller;
import com.dain_torson.httpclient.controller.connection.rest.RESTConnection;
import com.dain_torson.httpclient.controller.connection.rpc.RPCConnection;
import com.dain_torson.httpclient.controller.connection.soap.SOAPConnection;
import com.dain_torson.httpclient.data.PersonDataContainerCreator;
import com.dain_torson.httpclient.data.PersonDataCreator;
import com.dain_torson.httpclient.data.SimplePersonDataCreator;
import com.dain_torson.httpclient.view.GUIManager;

public class ConnectionManager {

    private PersonDataContainerCreator currentCreator;
    private Connection currentConnection;
    private boolean connected;
    private GUIManager guiManager;

    public ConnectionManager(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    public void connect(ConnectionType type, String url, int port) throws Exception{
        if(type == ConnectionType.RPC) {
            currentConnection = new RPCConnection(this);
            currentCreator = new PersonDataCreator();
        }
        else if(type == ConnectionType.SOAP){
            currentConnection = new SOAPConnection(this);
            currentCreator = new SimplePersonDataCreator();
        }
        else {
            currentConnection = new RESTConnection(this);
            currentCreator = new SimplePersonDataCreator();
        }
        currentConnection.connect(url, port);
        connected = true;
    }

    public void disconnect() {
        currentConnection.disconnect();
        currentConnection = null;
        currentCreator = null;
        connected = false;
    }

    public void handleError() {
        disconnect();
        guiManager.clearGui();
    }

    public PersonDataContainerCreator getCurrentCreator() {
        return currentCreator;
    }

    public Connection getCurrentConnection() {
        return currentConnection;
    }

    public boolean isConnected() {
        return connected;
    }

    public enum ConnectionType {RPC, SOAP, REST}
}
