package com.dain_torson.httpclient.controller.connection;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.multithreading.RPCGetAllThread;
import com.dain_torson.httpclient.controller.connection.rpc.multithreading.RPCGetByFioThread;
import com.dain_torson.httpclient.controller.connection.rpc.multithreading.RPCGetByPatternThread;
import com.dain_torson.httpclient.controller.connection.rpc.multithreading.RPCNewRecordThread;
import com.dain_torson.httpclient.data.PersonData;
import com.dain_torson.httpclient.data.PersonDataContainer;

import java.util.ArrayList;
import java.util.List;

public abstract class Connection {

    protected ConnectionManager manager;

    public Connection(ConnectionManager manager) {
        this.manager = manager;
    }

    public abstract void connect(String url, int port) throws Exception;
    public abstract void disconnect();

    protected abstract Thread createGetAllThread(UpdateGUIRunnableBuilder builder);
    protected abstract Thread createGetByFioThread(String fio, UpdateGUIRunnableBuilder builder);
    protected abstract Thread createGetByPatternThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder);
    protected abstract Thread createNewRecordThread(PersonDataContainer data, UpdateGUIRunnableBuilder builder);
    protected abstract Thread createDeleteRecordThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder);
    protected abstract Thread createEditRecordThread(PersonDataContainer oldData, PersonDataContainer newData,
                                                     UpdateGUIRunnableBuilder builder);

    private void prepareAndStart(Thread thread) {
        thread.setDaemon(true);
        thread.setUncaughtExceptionHandler(new DataTransferExceptionHandler(manager));
        thread.start();
    }

    public void getAll(UpdateGUIRunnableBuilder builder) {
        Thread thread = createGetAllThread(builder);
        prepareAndStart(thread);
    }

    public void getByFio(String fio, UpdateGUIRunnableBuilder builder) {
        Thread thread = createGetByFioThread(fio, builder);
        prepareAndStart(thread);
    }

    public void getByPattern(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        Thread thread = createGetByPatternThread(pattern, builder);
        prepareAndStart(thread);
    }

    public void newRecord(PersonDataContainer data, UpdateGUIRunnableBuilder builder) {
        Thread thread = createNewRecordThread(data, builder);
        prepareAndStart(thread);
    }

    public void deleteRecord(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        Thread thread = createDeleteRecordThread(pattern, builder);
        prepareAndStart(thread);
    }

    public void editRecord(PersonDataContainer oldData, PersonDataContainer newData, UpdateGUIRunnableBuilder builder) {
        Thread thread = createEditRecordThread(oldData, newData, builder);
        prepareAndStart(thread);
    }
}

