package com.dain_torson.httpclient.controller.connection.soap;

import com.dain_torson.httpclient.controller.connection.Connection;
import com.dain_torson.httpclient.controller.connection.ConnectionManager;
import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.soap.multithreading.*;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.SOAPInfoService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;

public class SOAPConnection extends Connection {

    private SOAPInfoService servicePort;

    public SOAPConnection(ConnectionManager manager) {
        super(manager);
    }

    @Override
    public void connect(String url, int port) throws Exception{
        String servletUrl = url + ":" + port + "/StudentInfoServices/soap/info?wsdl";
        URL urlObject;
        try {
            urlObject = new URL(servletUrl);
        }
        catch (MalformedURLException ex){
            ex.printStackTrace();
            return;
        }

        QName qname = new QName("http://info.soap.servlets.dain_torson.com/", "SOAPInfoServiceImplService");
        QName portName = new QName("http://info.soap.servlets.dain_torson.com/", "SOAPInfoServiceImplPort");

        Service service;
        service = Service.create(urlObject, qname);
        servicePort = service.getPort(portName, SOAPInfoService.class);
    }

    @Override
    public void disconnect() {
        servicePort = null;
    }

    @Override
    protected Thread createGetAllThread(UpdateGUIRunnableBuilder builder) {
        return new SOAPGetAllThread(servicePort, builder);
    }

    @Override
    protected Thread createGetByFioThread(String fio, UpdateGUIRunnableBuilder builder) {
        return new SOAPGetByFioThread(servicePort, builder, fio);
    }

    @Override
    protected Thread createGetByPatternThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new SOAPGetByPatternThread(servicePort, builder, (SimplePersonData)pattern);
    }

    @Override
    protected Thread createNewRecordThread(PersonDataContainer data, UpdateGUIRunnableBuilder builder) {
        return new SOAPNewRecordThread(servicePort, builder, (SimplePersonData)data);
    }

    @Override
    protected Thread createDeleteRecordThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new SOAPDeleteRecordThread(servicePort, builder, (SimplePersonData)pattern);
    }

    @Override
    protected Thread createEditRecordThread(PersonDataContainer oldData, PersonDataContainer newData, UpdateGUIRunnableBuilder builder) {
        return new SOAPEditRecordThread(servicePort, builder, (SimplePersonData)oldData, (SimplePersonData) newData);
    }

}
