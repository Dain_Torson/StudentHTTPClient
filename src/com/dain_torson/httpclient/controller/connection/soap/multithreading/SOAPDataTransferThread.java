package com.dain_torson.httpclient.controller.connection.soap.multithreading;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.RPCInfoService;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.SOAPInfoService;
import javafx.application.Platform;

import java.util.List;
import java.util.stream.Collectors;

public abstract class SOAPDataTransferThread extends Thread{

    protected SOAPInfoService client;
    private UpdateGUIRunnableBuilder builder;
    private Object param1;
    private Object param2;

    public SOAPDataTransferThread(SOAPInfoService client, UpdateGUIRunnableBuilder builder, Object param1) {
        this.client = client;
        this.param1 = param1;
        this.builder = builder;
    }


    public SOAPDataTransferThread(SOAPInfoService client, UpdateGUIRunnableBuilder builder,
                                  Object param1, Object param2) {
        this.client = client;
        this.param1 = param1;
        this.param2 = param2;
        this.builder = builder;
    }

    protected abstract List<SimplePersonData> clientOperation(Object param1, Object param2);

    @Override
    public void run(){
        List<SimplePersonData> data;
        data = clientOperation(param1, param2);

        List<PersonDataContainer> result = data.stream().map(value -> (PersonDataContainer) value)
                .collect(Collectors.toList());
        builder.setPersonList(result);
        Platform.runLater(builder.build());
    }
}