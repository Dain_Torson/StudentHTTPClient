package com.dain_torson.httpclient.controller.connection.soap.multithreading;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.SOAPInfoService;

import java.util.List;

public class SOAPGetAllThread extends SOAPDataTransferThread{
    public SOAPGetAllThread(SOAPInfoService client, UpdateGUIRunnableBuilder builder) {
        super(client, builder,null);
    }

    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) {
        return client.getAll();
    }
}
