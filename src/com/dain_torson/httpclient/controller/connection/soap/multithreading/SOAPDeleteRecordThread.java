package com.dain_torson.httpclient.controller.connection.soap.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.SOAPInfoService;

import java.util.List;

public class SOAPDeleteRecordThread extends SOAPDataTransferThread{
    public SOAPDeleteRecordThread(SOAPInfoService client, UpdateGUIRunnableBuilder builder, SimplePersonData param1) {
        super(client, builder, param1);
    }

    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) {
        client.deleteRecord((SimplePersonData)param1);
        return client.getAll();
    }
}
