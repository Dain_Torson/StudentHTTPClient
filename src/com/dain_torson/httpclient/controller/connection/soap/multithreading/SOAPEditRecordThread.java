package com.dain_torson.httpclient.controller.connection.soap.multithreading;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.SOAPInfoService;

import java.util.List;

public class SOAPEditRecordThread extends SOAPDataTransferThread{
    public SOAPEditRecordThread(SOAPInfoService client, UpdateGUIRunnableBuilder builder,
                                SimplePersonData param1, SimplePersonData param2) {
        super(client, builder, param1, param2);
    }

    @Override
    protected List<SimplePersonData> clientOperation(Object param1, Object param2) {
        client.editRecord((SimplePersonData)param1, (SimplePersonData)param2);
        return client.getAll();
    }
}
