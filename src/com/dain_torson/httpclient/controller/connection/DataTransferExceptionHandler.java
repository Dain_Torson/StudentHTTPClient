package com.dain_torson.httpclient.controller.connection;

import javafx.application.Platform;

public class DataTransferExceptionHandler implements Thread.UncaughtExceptionHandler{

    private ConnectionManager manager;

    public DataTransferExceptionHandler(ConnectionManager manager) {
        this.manager = manager;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        Platform.runLater(manager::handleError);
    }
}
