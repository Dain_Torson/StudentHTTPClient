package com.dain_torson.httpclient.controller.connection.guiupdating;

import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.view.TablePane;

import java.util.List;

public class UpdateGUIRunnable implements Runnable{

    private TablePane pane;
    private List<PersonDataContainer> personList;

    public UpdateGUIRunnable(TablePane pane, List<PersonDataContainer> personList) {
        this.pane = pane;
        this.personList = personList;
    }

    @Override
    public void run() {
        pane.setPersonData(personList);
    }
}
