package com.dain_torson.httpclient.controller.connection.guiupdating;

import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.view.TablePane;

import java.util.List;

public class UpdateGUIRunnableBuilder {

    private TablePane pane;
    private List<PersonDataContainer> personList;

    public UpdateGUIRunnableBuilder(TablePane pane) {
        this.pane = pane;
    }

    public void setPersonList(List<PersonDataContainer> personList) {
        this.personList = personList;
    }

    public UpdateGUIRunnable build(){
        if(pane == null || personList == null) return null;
        return new UpdateGUIRunnable(pane, personList);
    }
}
