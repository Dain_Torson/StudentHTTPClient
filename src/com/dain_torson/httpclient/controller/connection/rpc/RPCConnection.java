package com.dain_torson.httpclient.controller.connection.rpc;


import com.dain_torson.httpclient.controller.connection.Connection;
import com.dain_torson.httpclient.controller.connection.ConnectionManager;
import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.multithreading.*;
import com.dain_torson.httpclient.data.PersonData;
import com.dain_torson.httpclient.data.PersonDataContainer;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TTransportException;


public class RPCConnection extends Connection {

    private THttpClient thc;
    private RPCInfoService.Client client;

    public RPCConnection(ConnectionManager manager) {
        super(manager);
    }

    @Override
    public void connect(String url, int port) throws Exception{
        String servletUrl = url + ":" + port + "/StudentInfoServices/rpc/info";
        thc = new THttpClient(servletUrl);
        TProtocol loPFactory = new TCompactProtocol(thc);
        client = new RPCInfoService.Client(loPFactory);
    }

    @Override
    public void disconnect() {
        thc.close();
    }

    @Override
    protected Thread createGetAllThread(UpdateGUIRunnableBuilder builder) {
        return new RPCGetAllThread(client, builder);
    }

    @Override
    protected Thread createGetByFioThread(String fio, UpdateGUIRunnableBuilder builder) {
        return new RPCGetByFioThread(client, builder, fio);
    }

    @Override
    protected Thread createGetByPatternThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new RPCGetByPatternThread(client, builder, (PersonData)pattern);
    }

    @Override
    protected Thread createNewRecordThread(PersonDataContainer data, UpdateGUIRunnableBuilder builder) {
        return new RPCNewRecordThread(client, builder, (PersonData)data);
    }

    @Override
    protected Thread createDeleteRecordThread(PersonDataContainer pattern, UpdateGUIRunnableBuilder builder) {
        return new RPCDeleteRecordThread(client, builder, (PersonData)pattern);
    }

    @Override
    protected Thread createEditRecordThread(PersonDataContainer oldData, PersonDataContainer newData, UpdateGUIRunnableBuilder builder) {
        return new RPCEditRecordThread(client, builder, (PersonData)oldData, (PersonData) newData);
    }


}
