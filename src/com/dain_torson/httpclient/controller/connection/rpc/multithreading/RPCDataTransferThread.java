package com.dain_torson.httpclient.controller.connection.rpc.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.RPCInfoService;
import com.dain_torson.httpclient.data.PersonData;
import com.dain_torson.httpclient.data.PersonDataContainer;
import javafx.application.Platform;
import org.apache.thrift.TException;

import java.util.List;
import java.util.stream.Collectors;

public abstract class RPCDataTransferThread extends Thread{

    protected RPCInfoService.Client client;
    private UpdateGUIRunnableBuilder builder;
    private Object param1;
    private Object param2;

    public RPCDataTransferThread(RPCInfoService.Client client,
            UpdateGUIRunnableBuilder builder, Object param1) {
        this.client = client;
        this.param1 = param1;
        this.builder = builder;
    }

    public RPCDataTransferThread(RPCInfoService.Client client, UpdateGUIRunnableBuilder builder,
                                 Object param1, Object param2) {
        this.client = client;
        this.param1 = param1;
        this.param2 = param2;
        this.builder = builder;
    }

    protected abstract List<PersonData> clientOperation(Object param1, Object param2) throws TException;

    @Override
    public void run() {
        List<PersonData> data;

        try {
            data = clientOperation(param1, param2);
        } catch (TException ex) {
            throw new RuntimeException("Smth bad happened");
        }

        List<PersonDataContainer> result = data.stream().map(value -> (PersonDataContainer) value)
                .collect(Collectors.toList());
        builder.setPersonList(result);
        Platform.runLater(builder.build());
    }
}