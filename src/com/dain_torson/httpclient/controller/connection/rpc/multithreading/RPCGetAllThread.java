package com.dain_torson.httpclient.controller.connection.rpc.multithreading;


import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.RPCInfoService;
import com.dain_torson.httpclient.data.PersonData;
import com.dain_torson.httpclient.data.PersonDataContainer;
import org.apache.thrift.TException;

import java.util.List;

public class RPCGetAllThread extends RPCDataTransferThread {

    public RPCGetAllThread(RPCInfoService.Client client, UpdateGUIRunnableBuilder builder) {
        super(client, builder, null);
    }

    @Override
    protected List<PersonData> clientOperation(Object param1, Object param2) throws TException {
        return client.getAll();
    }
}
