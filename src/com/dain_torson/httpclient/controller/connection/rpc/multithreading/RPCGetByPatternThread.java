package com.dain_torson.httpclient.controller.connection.rpc.multithreading;

import com.dain_torson.httpclient.controller.connection.guiupdating.UpdateGUIRunnableBuilder;
import com.dain_torson.httpclient.controller.connection.rpc.RPCInfoService;
import com.dain_torson.httpclient.data.PersonData;
import com.dain_torson.httpclient.data.PersonDataContainer;
import org.apache.thrift.TException;

import java.util.List;

public class RPCGetByPatternThread extends RPCDataTransferThread {
    public RPCGetByPatternThread(RPCInfoService.Client client, UpdateGUIRunnableBuilder builder, PersonData param) {
        super(client, builder, param);
    }

    @Override
    protected List<PersonData> clientOperation(Object param1, Object param2) throws TException {
        return client.getByPattern((PersonData)param1);
    }
}
