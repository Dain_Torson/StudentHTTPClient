package com.dain_torson.httpclient.controller.msgbox;

import com.dain_torson.httpclient.controller.connection.ConnectionManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class ConnectMsgBoxController extends MsgBoxController{

    private static final String fxmlRoot = "view/msgbox/connectMsgBoxView.fxml";
    private static final String title = "Connect to";
    private static final int width = 200;
    private static final int height = 150;

    private static String url;
    private static int port;
    private static ConnectionManager.ConnectionType type;

    @FXML
    private TextField urlField;
    @FXML
    private TextField portField;
    @FXML
    private ChoiceBox<String> connectChoiceBox;

    public ConnectMsgBoxController() {
        super(title, fxmlRoot, width, height);
        url = "";
        port = 0;
        type = null;
    }

    private boolean gatherData() {
        url = urlField.getText();
        try {
            port = Integer.parseInt(portField.getText());
        }
        catch (NumberFormatException ex) {
            portField.setText("8080");
            return false;
        }
        String value  = connectChoiceBox.getValue();
        if(value.equals("Thrift")) {
            type = ConnectionManager.ConnectionType.RPC;
        }
        else if(value.equals("SOAP")){
            type = ConnectionManager.ConnectionType.SOAP;
        }
        else {
            type = ConnectionManager.ConnectionType.REST;
        }
        return true;
    }

    private void close() {
        Stage stage = (Stage) urlField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onOkButtonPressed() {
        if(!gatherData()) return;
        close();
    }

    @FXML
    private void onCancelButtonPressed() {
        close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public String getUrl() {
        return url;
    }

    public int getPort() {
        return port;
    }

    public ConnectionManager.ConnectionType getType() {
        return type;
    }
}
