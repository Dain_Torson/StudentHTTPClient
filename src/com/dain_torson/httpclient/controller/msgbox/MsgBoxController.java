package com.dain_torson.httpclient.controller.msgbox;


import com.dain_torson.httpclient.Main;
import com.dain_torson.httpclient.controller.Controller;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public abstract class MsgBoxController implements Initializable{

    private Stage stage;
    private String title;
    private String fxmlRoot;
    private int width;
    private int height;

    public MsgBoxController(String title, String fxmlRoot, int width, int height){
        this.title = title;
        this.fxmlRoot = fxmlRoot;
        this.width = width;
        this.height = height;
    }

    public void show(Window owner) {
        if (stage == null) {
            Parent root = null;
            try {
                root = FXMLLoader.load(Main.class.getResource(fxmlRoot));
            } catch (IOException ex) {
                ex.printStackTrace();
                return;
            }
            Scene scene = new Scene(root, width, height);
            stage = new Stage();
            stage.setScene(scene);
            stage.setTitle(title);
            stage.initModality(Modality.NONE);
        }

        stage.showAndWait();
        stage.toFront();
    }
}
