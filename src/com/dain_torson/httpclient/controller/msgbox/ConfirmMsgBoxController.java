package com.dain_torson.httpclient.controller.msgbox;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ConfirmMsgBoxController extends MsgBoxController{

    private static String title = "Confirmation";
    private static String fxmlRoot = "view/msgbox/confirmMsgBoxView.fxml";
    private static int width = 200;
    private static int height = 150;

    private static String msg = "Are you sure?";
    private static boolean confirmed;

    @FXML
    private Label msgLabel;

    public ConfirmMsgBoxController() {
        super(title, fxmlRoot, width, height);
        confirmed = false;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public static void setMsg(String msg) {
        ConfirmMsgBoxController.msg = msg;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        msgLabel.setText(msg);
    }

    private void close() {
        Stage stage = (Stage) msgLabel.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onOKButtonPressed() {
        confirmed = true;
        close();
    }

    @FXML
    private void onCancelButtonPressed() {
        close();
    }
}
