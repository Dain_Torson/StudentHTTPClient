package com.dain_torson.httpclient.controller.msgbox;


import com.dain_torson.httpclient.data.PersonDataContainer;
import com.dain_torson.httpclient.data.PersonDataContainerCreator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ProcessRecordMsgBoxController extends MsgBoxController{

    private static final String fxmlRoot = "view/msgbox/processMsgBoxView.fxml";
    private static final String title = "Enter data";
    private static final int width = 300;
    private static final int height = 300;

    private static PersonDataContainer enteredData;
    private static PersonDataContainer pattern;
    private static PersonDataContainerCreator creator;

    @FXML
    private TextField nameField;
    @FXML
    private TextField groupField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField ageField;
    @FXML
    private TextField gradeField;
    @FXML
    ChoiceBox<String> genderChoiceBox;
    @FXML
    ChoiceBox<String> eduChoiceBox;

    public ProcessRecordMsgBoxController() {
        super(title, fxmlRoot, width, height);
        enteredData = null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(pattern != null) {
            nameField.setText(pattern.getFio());
            groupField.setText(pattern.getGroup());
            cityField.setText(pattern.getCity());
            ageField.setText(String.valueOf(pattern.getAge()));
            gradeField.setText(String.valueOf(pattern.getAvgGrade()));
            genderChoiceBox.setValue(pattern.getGender());
            eduChoiceBox.setValue(pattern.getEduForm());
            return;
        }
        ageField.setText("0");
        gradeField.setText("0.0");
    }

    private boolean gatherData() {
        enteredData = creator.create();
        enteredData.setFio(nameField.getText());
        enteredData.setGroup(groupField.getText());
        enteredData.setCity(cityField.getText());
        enteredData.setEduForm(eduChoiceBox.getValue());
        enteredData.setGender(genderChoiceBox.getValue());

        int age;
        try {
            age = Integer.parseInt(ageField.getText());
        }
        catch (NumberFormatException ex) {
            ageField.setText("Wrong Input");
            return false;
        }

        double grade;
        try {
            grade = Double.parseDouble(gradeField.getText());
        }
        catch (NumberFormatException ex) {
            gradeField.setText("Wrong Input");
            return false;
        }

        enteredData.setAge(age);
        enteredData.setAvgGrade(grade);
        return true;
    }

    private void close() {
        Stage stage = (Stage) nameField.getScene().getWindow();
        pattern = null;
        creator = null;
        stage.close();
    }

    @FXML
    private void onOkButtonPressed() {
        if(creator == null || !gatherData()) return;
        close();
    }

    @FXML
    private void onCancelButtonPressed() {
        close();
    }

    public PersonDataContainer getEnteredData() {
        return enteredData;
    }

    public static void setCreator(PersonDataContainerCreator containerCreator) {
        creator = containerCreator;
    }

    public static void setPattern(PersonDataContainer personPattern) {
        pattern = personPattern;
    }
}
