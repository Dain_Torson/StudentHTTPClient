package com.dain_torson.httpclient.data;

public interface PersonDataContainerCreator {
    PersonDataContainer create();
    PersonDataContainer createDummy();
}
