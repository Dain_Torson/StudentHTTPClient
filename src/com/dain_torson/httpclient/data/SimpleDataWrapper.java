package com.dain_torson.httpclient.data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class SimpleDataWrapper {

    private SimplePersonData first;
    private SimplePersonData second;

    public SimplePersonData getFirst() {
        return first;
    }

    public void setFirst(SimplePersonData first) {
        this.first = first;
    }

    public SimplePersonData getSecond() {
        return second;
    }

    public void setSecond(SimplePersonData second) {
        this.second = second;
    }
}
