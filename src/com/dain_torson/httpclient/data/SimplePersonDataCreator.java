package com.dain_torson.httpclient.data;

public class SimplePersonDataCreator implements PersonDataContainerCreator{
    @Override
    public SimplePersonData create() {
        return new SimplePersonData();
    }

    @Override
    public PersonDataContainer createDummy() {
        SimplePersonData dummy = new SimplePersonData();
        dummy.setFio("");
        dummy.setGroup("");
        dummy.setCity("");
        dummy.setEduForm("");
        dummy.setGender("");
        dummy.setAvgGrade(0);
        dummy.setAge(0);
        return dummy;
    }
}
