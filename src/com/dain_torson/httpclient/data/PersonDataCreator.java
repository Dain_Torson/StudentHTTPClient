package com.dain_torson.httpclient.data;

public class PersonDataCreator implements PersonDataContainerCreator {
    @Override
    public PersonData create() {
        return new PersonData();
    }

    @Override
    public PersonDataContainer createDummy() {
        PersonData dummy = new PersonData();
        dummy.setFio("");
        dummy.setGroup("");
        dummy.setCity("");
        dummy.setEduForm("");
        dummy.setGender("");
        dummy.setAvgGrade(0);
        dummy.setAge(0);
        return dummy;
    }
}
