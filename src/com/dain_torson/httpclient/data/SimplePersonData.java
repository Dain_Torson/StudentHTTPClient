package com.dain_torson.httpclient.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimplePersonData implements PersonDataContainer{

    private String fio;
    private String group;
    private String eduForm;
    private String gender;
    private String city;
    private int age;
    private double avgGrade;

    public String getFio() {
        return fio;
    }

    public PersonDataContainer setFio(String fio) {
        this.fio = fio;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public PersonDataContainer setGroup(String group) {
        this.group = group;
        return this;
    }

    public String getEduForm() {
        return eduForm;
    }

    public PersonDataContainer setEduForm(String eduForm) {
        this.eduForm = eduForm;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public PersonDataContainer setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getCity() {
        return city;
    }

    public PersonDataContainer setCity(String city) {
        this.city = city;
        return this;
    }

    public int getAge() {
        return age;
    }

    public PersonDataContainer setAge(int age) {
        this.age = age;
        return this;
    }

    public double getAvgGrade() {
        return avgGrade;
    }

    public PersonDataContainer setAvgGrade(double avgGrade) {
        this.avgGrade = avgGrade;
        return this;
    }
}
