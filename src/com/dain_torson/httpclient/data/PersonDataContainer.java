package com.dain_torson.httpclient.data;

public interface PersonDataContainer {
    public String getFio();
    public PersonDataContainer setFio(String fio);
    public String getGroup();
    public PersonDataContainer setGroup(String group);
    public String getEduForm();
    public PersonDataContainer setEduForm(String eduForm);
    public String getGender();
    public PersonDataContainer setGender(String gender);
    public String getCity();
    public PersonDataContainer setCity(String city);
    public int getAge();
    public PersonDataContainer setAge(int age);
    public double getAvgGrade();
    public PersonDataContainer setAvgGrade(double avgGrade);
}
